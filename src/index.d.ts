import { Common } from "./clover-px.common";
export declare class CloverPx extends Common {
    public account(): Promise<any>;

    public checkout(publicKey: string, preferenceId: string): Promise<any>;
   
}
